const path = require("path");
const SpotifyWebApi = require("spotify-web-api-node");
var config = require("./config.json")
const express = require('express')
const app = express()
const openurl = require("openurl")
const fs = require("fs")
const querystring = require('querystring');
const url = require('url');
const request = require("request")
const updater = require("./etc/updater")
const bodyParser = require("body-parser");

var stop = false;
var accessToken;
var rToken;
var codeToken;
var refresher;
var running = false;
var address = "http://127.0.0.1/"
if (config.port !== 80) {
    address = "http://127.0.0.1:" + config.port + "/"
}
if (config.checkForUpdates) updater.handle();
//https://github.com/SteelSeries/gamesense-sdk/blob/master/doc/api/writing-handlers-in-json.md
//https://www.npmjs.com/package/spotify-web-api-node
//https://www.reddit.com/r/steelseries/comments/jh8bz0/spotify_app_for_the_oled/
//https://gitlab.com/Unbemerkt/_uo_steelseries_spotify_integration
var spotifyApi = new SpotifyWebApi({
    clientId: config.clientID,
    clientSecret: config.clientSecret,
    redirectUri: address
})
var steelSeriesCfg = JSON.parse(fs.readFileSync(config.coreProps_PATH, "utf8"));

var server = app.listen(config.port)
registerDef();
if (!config.isRegistered) registerEngine();
var loginOK = fs.readFileSync("./html/loginOK.html", {encoding: "utf8", flag: "r"});
var loginFailed = fs.readFileSync("./html/loginFailed.html", {encoding: "utf8", flag: "r"});

app.get('/login', function (req, res) {
    var scopes = 'user-read-currently-playing';
    res.redirect('https://accounts.spotify.com/authorize' +
        '?response_type=code' +
        '&client_id=' + config.clientID +
        (scopes ? '&scope=' + encodeURIComponent(scopes) : '') +
        '&redirect_uri=' + encodeURIComponent(address + "login_confirm/"));
});


app.get("/login_confirm", (req, res) => {
    let parsedURl = url.parse(req.url);
    let parsedQ = querystring.parse(parsedURl.query);
    var error = parsedQ.error;
    var code = parsedQ.code;
    if (error !== undefined) {
        res.send(loginFailed.replace(/%CODE%/g, error));
    } else if (code !== undefined) {
        // your application requests authorization
        codeToken = code;
        refreshToken(res, false);
    } else {
        res.send(loginFailed.replace(/%CODE%/g, "AN_ERROR_OCCURRED"))
    }

});
openurl.open(address + "login")


function refreshToken(res, refresh) {
    var c;
    if (refresh) {
        c = rToken;
    } else {
        c = codeToken
    }
    var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        headers: {
            Authorization:
                'Basic ' + Buffer.from(config.clientID + ':' + config.clientSecret).toString('base64')
        },
        form: {
            grant_type: 'authorization_code',
            code: c,
            redirect_uri: address + "login_confirm/"
        },
        json: true
    };

    request.post(authOptions, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            //console.log("1: ",body)
            if (res !== undefined) {
                res.send(loginOK.replace(/%CODE%/g, body.access_token).replace(/%AUTO_START%/g, config.autoStart));
            }
            accessToken = body.access_token;
            spotifyApi.setAccessToken(body.access_token);
            rToken = body.refresh_token;
            spotifyApi.setRefreshToken(rToken);
            refresher = setTimeout(tokenRefresher, 3480000);
        } else {
            if (res !== undefined) res.send(loginFailed.replace(/%CODE%/g, "AN_ERROR_OCCURRED"));
            console.log("(" + response.statusCode + ") error: ", error)
        }
    });
}

function tokenRefresher(){
    if (rToken===undefined)return;
    // clientId, clientSecret and refreshToken has been set on the api object previous to this call.
    spotifyApi.refreshAccessToken().then(
        function(data) {
            console.log('The access token has been refreshed!');

            // Save the access token so that it's used in future calls
            spotifyApi.setAccessToken(data.body['access_token']);
            refresher = setTimeout(tokenRefresher, 3480000);
        },
        function(err) {
            console.log('Could not refresh access token', err);
        }
    );

}
function reload(){
    stop = true;
    console.log("Reloading...")
    setTimeout(init, 4000);
}
function registerDef() {
    app.use('/assets', express.static(path.join(__dirname, '\\html\\assets')));

    var html = fs.readFileSync("./html/dashboard.html", "utf8");
    var settingsHtml = fs.readFileSync("./html/settings.html", "utf8");
    app.get("/reload", (req, res) => {
        res.send(JSON.stringify({TYPE: "OK"}));
        reload();
    });
    app.get("/stop", (req, res) => {
        stop = true;
        running = false;
        res.send(JSON.stringify({TYPE: "OK"}));
        clearTimeout(refresher)
        console.log("Closing WebServer...");
        server.close();
    });
    app.get("/uninstall", ((req, res) => {
        stop = true;
        running = false;
        clearTimeout(refresher)
        console.log("Removing game from SSE...")
        sendData("remove_game", {"game": "SPOTIFY_UO"})
        res.send(JSON.stringify({TYPE: "OK"}));

        config.isRegistered = false;
        fs.writeFileSync("./config.json", JSON.stringify(config, null, 2), "utf8");
        console.log("Closing WebServer...");
        server.close();

    }));
    app.get("/", ((req, res) => {
        html = fs.readFileSync("./html/dashboard.html", "utf8");
        res.send(html.replace(/%AUTO_RELOAD%/g, (config.autoStart && !running)+"").replace(/%AUTO_START%/g, config.autoStart).replace(/%IS_RUNNING%/g, running)
            .replace(/%UPDATE_TIME%/g,config.screenUpdateTime+""));
    }));
    app.get("/settings", ((req, res) => {
        settingsHtml = fs.readFileSync("./html/settings.html", "utf8");
        res.send(settingsHtml.replace(/%AUTO_RELOAD%/g, (config.autoStart && !running)+"").replace(/%AUTO_START%/g, config.autoStart).replace(/%IS_RUNNING%/g, running));
    }));
    app.get("/config", ((req, res) => {
        res.status(200).json(config);
    }));
    app.post("/config", bodyParser.urlencoded({ extended: true }),(req, res) => {
        //save config
        console.log(req.body);
        config = JSON.parse(JSON.stringify(req.body.data));
        fs.writeFile("config.json",JSON.stringify(req.body.data,null,2),()=>{
            res.status(200).json(config);
        })

    });
    app.get("/data", ((req, res) => {
        res.status(200).json(track);
    }));
    console.log("Dashboard: " + address);
}

function init() {
    stop = false;
    running = true;
    updateSpotify();
    updateScreen();
    fs.writeFileSync(__dirname + "/remove.bat", 'echo "Removing UO SteelSeries - Spotify Integration"\n' +
        'del "' + __dirname + '\\" /s /f', "utf8");
    fs.writeFileSync(__dirname + "/remove.sh", 'rm "' + __dirname + '\\" -rf', "utf8");
}

var lastString = "";
var i2 = 0;
function getString(string) {
    //console.log("In: ",string)
    if (string !== lastString) {
        i2 = 0;
        lastString = string;
    }
    string = string.split('');
    var res = "";
    if (i2 >= (string.length)) {
        i2 = 0;
    }
    if (string.length <= 20) {
        i2 = 0;
    }
    var i4 = 0;
    for (var i3 = 0; i3 < 20 && i3 < string.length; i3++) {
        if (string[i3 + i2] !== undefined) {
            res += string[i3 + i2];
            i4++;
        }
    }
    res += "   ";
    for (var i5 = 0; i5 < 15 - i4; i5++) {
        if (string[i5] !== undefined) {
            res += string[i5];
        }
    }

    i2++;
    //console.log("out "+i2+": ",res);
    return res;
}

var track = {
    ID: undefined,
    ARTISTS: [],
    DURATION_MS: 0,
    PROGRESS: 0,
    PROGRESS_MS: 0,
    NAME: "Unknown Title",
    IS_PLAYING: false,
}

function updateTrack(id, bool) {
    track.IS_PLAYING = bool;
    if (id===null)return;
    if (track.ID === id) return;
    track.ID = id;
    try {
        spotifyApi.getTrack(id).then((data) => {
            track.DURATION_MS = data.body.duration_ms;
            track.ARTISTS = data.body.artists;
            track.IS_PLAYING = bool;
        });
    }catch (e){
        console.log("==================================");
        console.log("Something went wrong! You can ignore this error");
        console.log("----------------------------------");
        console.log(e);
        console.log("==================================");
    }
}



function updateScreen() {
    if (stop) return;
    if (track.IS_PLAYING) {
        var artistsString = "";
        if (track.ARTISTS !== undefined && track.ARTISTS.length === 1) {
            if (track.ARTISTS[0] !== undefined && track.ARTISTS[0].name !== undefined) artistsString = track.ARTISTS[0].name
        } else if (track.ARTISTS !== undefined && track.ARTISTS.length > 0) {
            for (var i = 0; i < track.ARTISTS.length; i++) {
                if (i === 0) {
                    if (track.ARTISTS[i] !== undefined && track.ARTISTS[i].name !== undefined) artistsString += track.ARTISTS[i].name
                } else {
                    if (track.ARTISTS[i] !== undefined && track.ARTISTS[i].name !== undefined) artistsString += ", " + track.ARTISTS[i].name
                }
            }
        } else {
            artistsString = undefined;
        }
        var name = track.NAME;
        var line;
        if (artistsString === undefined) {
            line = getString(name);
        } else {
            line = getString(name + " -- " + artistsString);
        }


        var perc = Math.floor((track.PROGRESS_MS / track.DURATION_MS) * 100);
        track.PROGRESS = perc;
        sendData("game_event", {
            "game": "SPOTIFY_UO",
            "event": "SC_PROGRESS_UPDATE",
            "data": {
                "value": perc,
                "frame": {
                    "first-line": line,
                }
            }
        });
        setTimeout(updateScreen, config.screenUpdateTime);
    } else {
        var artistsString = "";
        if (track.ARTISTS !== undefined && track.ARTISTS.length === 1) {
            if (track.ARTISTS[0] !== undefined && track.ARTISTS[0].name !== undefined) artistsString = track.ARTISTS[0].name
        } else if (track.ARTISTS !== undefined && track.ARTISTS.length > 0) {
            for (var i = 0; i < track.ARTISTS.length; i++) {
                if (i === 0) {
                    if (track.ARTISTS[i] !== undefined && track.ARTISTS[i].name !== undefined) artistsString += track.ARTISTS[i].name
                } else {
                    if (track.ARTISTS[i] !== undefined && track.ARTISTS[i].name !== undefined) artistsString += ", " + track.ARTISTS[i].name
                }
            }
        } else {
            artistsString = undefined;
        }
        var line;
        if (artistsString === undefined) {
            line = getString(track.NAME);
        } else {
            line = getString(track.NAME + " -- " + artistsString);
        }

        sendData("game_event", {
            "game": "SPOTIFY_UO",
            "event": "SC_IDLE_STATE",
            "data": {
                "value": Math.floor(Math.random() * Math.floor(99)),
                "frame": {
                    "first-line": line,
                    "second-line": "  »  Paused",
                }
            }
        });
        setTimeout(updateScreen, config.screenUpdateTime);

    }
}

function updateSpotify() {
    if (stop) return;
    try {
        spotifyApi.getMyCurrentPlayingTrack().then(function (data) {
            if (data === undefined || data.body === undefined || data.body.item === undefined || data.body.item.name === undefined || data.body.progress_ms === undefined) {
                //sendData("game_event", {
                //    "game": "SPOTIFY_UO",
                //    "event": "SC_PROGRESS_UPDATE",
                //    "data": {
                //        "value": 0,
                //        "frame": {
                //            "first-line": "Unknown Title",
                //        }
                //    }
                //});
                track.IS_PLAYING = false;
                track.ARTISTS = undefined;
                track.NAME = "Unknown Title";
                updateTrack(null, false);
                setTimeout(updateSpotify, config.spotifyUpdateTime);
                return;
            }
            track.PROGRESS_MS = data.body.progress_ms;
            track.NAME = data.body.item.name;
            updateTrack(data.body.item.id, data.body.is_playing);
            setTimeout(updateSpotify, config.spotifyUpdateTime);
        }, function (err) {
            console.log("==================================");
            if (config.automaticReloadWhenError){
                console.log("Something went wrong! The app will be reloaded!");
            }else{
                console.log("Something went wrong! Please Reload the app!");
            }
            console.log("----------------------------------");
            console.log(err);
            console.log("==================================");
            if (config.automaticReloadWhenError)reload();
        });
    } catch (e) {
        console.log("==================================");
        console.log("Something went wrong! You can ignore this error");
        console.log("----------------------------------");
        console.log(e);
        console.log("==================================");
        setTimeout(updateSpotify, config.spotifyUpdateTime);
    }

}


function registerEngine() {
    sendData("game_metadata", {"game": "SPOTIFY_UO", "game_display_name": "Spotify", "developer": "Unbemerkt"});
    sendData("bind_game_event", {
        "game": "SPOTIFY_UO",
        "event": "SC_PROGRESS_UPDATE",
        "handlers": [
            {
                "device-type": config.screenSize,
                "zone": "one",
                "mode": "screen",
                "datas": [
                    {
                        "lines": [
                            {
                                "has-text": true,
                                "context-frame-key": "first-line",
                            },
                            {
                                "has-progress-bar": true
                            }
                        ]
                    }
                ]
            }
        ]
    })

    sendData("bind_game_event", {
        "game": "SPOTIFY_UO",
        "event": "SC_IDLE_STATE",
        "handlers": [
            {
                "device-type": config.screenSize,
                "zone": "one",
                "mode": "screen",
                "datas": [
                    {
                        "lines": [
                            {
                                "has-text": true,
                                "context-frame-key": "first-line",
                            },
                            {
                                "has-text": true,
                                "has-progress-bar": false,
                                "context-frame-key": "second-line",
                                "icon-id": 23
                            }
                        ]
                    }
                ]
            }
        ]
    })

    config.isRegistered = true;
    fs.writeFileSync("./config.json", JSON.stringify(config, null, 2), "utf8");
}

function sendData(endPoint, dataObj) {
    var authOptions = {
        json: dataObj
    };
    request.post("http://" + steelSeriesCfg.address + "/" + endPoint, authOptions, (error, response, body) => {
        //console.log(body)
        //console.log(error)
    })
}


