class SliderBTN {
    constructor(id, options) {
        this.id = id;
        this.options = options;
        const defaultOptions = {
            bgColor: "grey",
            btnColor: "rgba(255,0,0,0.73)",
            btnTextColor: "#ffffff",
            btnText: "",
            height: 30,
            width: 50,
            showArrow: true,
            arrowColor: "rgba(255,255,255,0.5)",
            hidden: false,
            btn: {
                border: {
                    enable: true,
                    color: "rgba(255,0,0,0.6)",
                    size: "1px",
                    type: "solid",
                    radius: "0px"
                },
            },
            line: {
                border: {
                    enable: false,
                    color: "rgb(255,0,0)",
                    size: "1px",
                    type: "solid",
                    radius: "0px"
                }
            }

        };
        if (options === undefined) {
            this.options = defaultOptions;
        } else {
            Object.keys(defaultOptions).forEach(key => {
                if (this.options[key] === undefined) {
                    this.options[key] = defaultOptions[key];
                }
            });
        }
        this.build();

        this.onFinish = () => {
        };
        this.sliderLine = document.getElementById(`${this.id}`);
        this.head = document.getElementById(`sliderBTN_${this.id}`);

        this.updateLineWidth();
        this.registerEvents();

        this.isDown = false;
        this.isFinished = false;
        this.canMove = true;
    }
    updateLineWidth(){
        this.sliderLineWidth = this.sliderLine.offsetWidth - this.head.offsetWidth;
    }

    build(bool) {
        if (bool){
            var borderStyle = "";
            if (this.options.line.border.enable){
                borderStyle = `
                border: ${this.options.line.border.size}  ${this.options.line.border.type} ${this.options.line.border.color}; 
                border-radius: ${this.options.line.border.radius};
            `;
            }
            var hidden = "";
            if (this.options.hidden){
                hidden = "display: none;"
            }
            document.getElementById(this.id).setAttribute("style", `height: ${this.options['height']}px; background-color: ${this.options['bgColor']};  ${borderStyle} ${hidden}`);
            this.updateLineWidth();
        }

        var html = ``;
        var arrow = "";
        if (this.options.showArrow) arrow = ` <i class='sliderBTN--arrow sliderBTN--arrow--right' style='border-color: ${this.options.arrowColor} !important;'></i>`;
        var borderStyle = "";
        if (this.options.btn.border.enable){
            borderStyle = `
                border: ${this.options.btn.border.size}  ${this.options.btn.border.type} ${this.options.btn.border.color}; 
                border-radius: ${this.options.btn.border.radius};
            `;
        }
        html += `<div class="sliderBTN--head" id="sliderBTN_${this.id}" 
                style="height: ${this.options['height']}px; width: ${this.options['width']}px; color: ${this.options['btnTextColor']}; background-color: ${this.options['btnColor']}; ${borderStyle}">
                    ${this.options['btnText']}${arrow}</div>`;
        document.getElementById(this.id).innerHTML = html;
        borderStyle = "";
        if (this.options.line.border.enable){
            borderStyle = `
                border: ${this.options.line.border.size}  ${this.options.line.border.type} ${this.options.line.border.color}; 
                border-radius: ${this.options.line.border.radius};
            `;
        }
        var hidden = "";
        if (this.options.hidden){
            hidden = "display: none;"
        }
        document.getElementById(this.id).setAttribute("style", `height: ${this.options['height']}px; background-color: ${this.options['bgColor']};  ${borderStyle} ${hidden}`);

    }


    moveBTNHead(event) {
        if (!this.isDown) return;
        if (!this.canMove) return;
        const newMargLeft = event.clientX - this.getPosition(this.sliderLine);
        if (newMargLeft >= 0 && newMargLeft <= this.sliderLineWidth) {
            this.head.style.marginLeft = newMargLeft + "px";
            this.isFinished = false;
        }
        if (newMargLeft < 0) {
            this.head.style.marginLeft = "0px";
            this.isFinished = false;
        }
        if (newMargLeft > this.sliderLineWidth) {
            this.head.style.marginLeft = this.sliderLineWidth + "px";
            this.isFinished = true;
        }
    }

    mouseDown() {
        this.isDown = true;

    }

    mouseUp(event) {
        if (this.isDown) {
            this.moveBTNHead(event);
            if (this.isFinished && this.canMove) {
                this.canMove = false;
                if (typeof this.onFinish === "function") this.onFinish();
            }
        }
        this.isDown = false;
    }

    setCallback(callback) {
        this.onFinish = callback;
    }

    getPosition(el) {
        return el.getBoundingClientRect().left;
    }

    registerEvents() {
        this.head.addEventListener('mousedown', this.mouseDown.bind(this), false);
        window.addEventListener('mouseup', this.mouseUp.bind(this), false);
        window.addEventListener('mousemove', this.moveBTNHead.bind(this), true);
    }

    getMe() {
        return this;
    }
    show(){
        this.options.hidden = false;
        this.build(true);

    }
}
