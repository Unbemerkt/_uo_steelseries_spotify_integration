const request = require("request");
const package = require("./../package.json")
module.exports = {
    handle: function (){

        var options = {
            url: 'https://gitlab.com/Unbemerkt/_uo_steelseries_spotify_integration/-/raw/master/package.json',
            json: true
        };

        request.get(options, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                if (body.version===package.version){
                    console.info("You application is up to date! ("+body.version+")")
                }else{
                    console.info("An Update is available! Please reinstall this application!");
                    console.info("Your Version: "+package.version);
                    console.info("Available Version: "+body.version);
                    console.info("Download: https://gitlab.com/Unbemerkt/_uo_steelseries_spotify_integration");
                }
            } else {
                console.log("Update Check failed: ", error)
            }
        });
    }
}