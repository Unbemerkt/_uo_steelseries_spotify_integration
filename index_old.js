const SpotifyWebApi = require("spotify-web-api-node");
const config = require("./config.json")
const express = require('express')
const app = express()
const openurl = require("openurl")
const fs = require("fs")
const querystring = require('querystring');
const url = require('url');
const request = require("request")
var stop = false;
var accessToken;
var rToken;
var codeToken;
var refresher;
var running = false;
var address = "http://127.0.0.1/"
if (config.port !== 80) {
    address = "http://127.0.0.1:" + config.port + "/"
}
//https://github.com/SteelSeries/gamesense-sdk/blob/master/doc/api/writing-handlers-in-json.md
//https://www.npmjs.com/package/spotify-web-api-node
//https://www.reddit.com/r/steelseries/comments/jh8bz0/spotify_app_for_the_oled/
//https://gitlab.com/Unbemerkt/_uo_steelseries_spotify_integration
var spotifyApi = new SpotifyWebApi({
    clientId: config.clientID,
    clientSecret: config.clientSecret,
    redirectUri: address
})
var steelSeriesCfg = JSON.parse(fs.readFileSync(config.coreProps_PATH, "utf8"));

var server = app.listen(config.port)
registerDef();
if (!config.isRegistered) registerEngine();
var loginOK = fs.readFileSync("./html/loginOK.html", {encoding: "utf8", flag: "r"});
var loginFailed = fs.readFileSync("./html/loginFailed.html", {encoding: "utf8", flag: "r"});

app.get('/login', function (req, res) {
    var scopes = 'user-read-currently-playing';
    res.redirect('https://accounts.spotify.com/authorize' +
        '?response_type=code' +
        '&client_id=' + config.clientID +
        (scopes ? '&scope=' + encodeURIComponent(scopes) : '') +
        '&redirect_uri=' + encodeURIComponent(address + "login_confirm/"));
});


app.get("/login_confirm", (req, res) => {
    let parsedURl = url.parse(req.url);
    let parsedQ = querystring.parse(parsedURl.query);
    var error = parsedQ.error;
    var code = parsedQ.code;

    if (error !== undefined) {
        res.send(loginFailed.replace(/%CODE%/g, error));
    } else if (code !== undefined) {
        // your application requests authorization
        codeToken = code;
        refreshToken(res, false);
    } else {
        res.send(loginFailed.replace(/%CODE%/g, "AN_ERROR_OCCURRED"))
    }

});
openurl.open(address + "login")


function refreshToken(res, refresh) {
    var c;
    if (refresh) {
        c = rToken;
    } else {
        c = codeToken
    }
    var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        headers: {
            Authorization:
                'Basic ' + Buffer.from(config.clientID + ':' + config.clientSecret).toString('base64')
        },
        form: {
            grant_type: 'authorization_code',
            code: c,
            redirect_uri: address + "login_confirm/"
        },
        json: true
    };

    request.post(authOptions, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            //console.log("1: ",body)
            if (res !== undefined) {
                res.send(loginOK.replace(/%CODE%/g, body.access_token).replace(/%AUTO_START%/g, config.autoStart));
            }
            accessToken = body.access_token;
            spotifyApi.setAccessToken(body.access_token);
            rToken = body.refresh_token;
            refresher = setTimeout(refreshToken, 3400000);
        } else {
            if (res !== undefined) res.send(loginFailed.replace(/%CODE%/g, "AN_ERROR_OCCURRED"));
            console.log("(" + response.statusCode + ") error: ", error)
        }
    });


}

function registerDef() {
    var html = fs.readFileSync("./html/dashboard_old.html", "utf8");
    app.get("/reload", (req, res) => {
        res.send(JSON.stringify({TYPE: "OK"}));
        stop = true;
        console.log("Reloading...")
        setTimeout(init, 4000);
    });
    app.get("/stop", (req, res) => {
        stop = true;
        running = false;
        res.send(JSON.stringify({TYPE: "OK"}));
        clearTimeout(refresher)
        console.log("Closing WebServer...");
        server.close();
    });
    app.get("/uninstall", ((req, res) => {
        stop = true;
        running = false;
        clearTimeout(refresher)
        console.log("Removing game from SSE...")
        sendData("remove_game", {"game": "SPOTIFY_UO"})
        res.send(JSON.stringify({TYPE: "OK"}));

        config.isRegistered = false;
        fs.writeFileSync("./config.json", JSON.stringify(config, null, 2), "utf8");
        console.log("Closing WebServer...");
        server.close();

    }));
    app.get("/", ((req, res) => {
        res.send(html.replace(/%AUTO_START%/g, config.autoStart).replace(/%IS_RUNNING%/g, running));
    }));
    console.log("Dashboard: " + address);
}

function init() {
    stop = false;
    running = true;
    updateSong();
    fs.writeFileSync(__dirname + "/remove.bat", 'echo "Removing UO SteelSeries - Spotify Integration"\n' +
        'del "' + __dirname + '\\" /s /f', "utf8");
    fs.writeFileSync(__dirname + "/remove.sh", 'rm "' + __dirname + '\\" -rf', "utf8");
}

var lastString = "";
var i2 = 0;

function getString(string) {
    //console.log("In: ",string)
    if (string !== lastString) {
        i2 = 0;
        lastString = string;
    }
    string = string.split('');
    var res = "";
    if (i2 >= (string.length)) {
        i2 = 0;
    }
    if (string.length <= 20) {
        i2 = 0;
    }
    var i4 = 0;
    for (var i3 = 0; i3 < 20 && i3 < string.length; i3++) {
        if (string[i3 + i2] !== undefined) {
            res += string[i3 + i2];
            i4++;
        }
    }
    res += "   ";
    for (var i5 = 0; i5 < 15 - i4; i5++) {
        if (string[i5] !== undefined) {
            res += string[i5];
        }
    }

    i2++;
    //console.log("out "+i2+": ",res);
    return res;
}

var track = {
    ID: undefined,
    ARTISTS: [],
    DURATION_MS: 0,
    PROGRESS: 0,
    NAME: "Unknown Title"
}

function updateTrack(id) {
    if (track.ID === id) return;
    track.ID = id;
    spotifyApi.getTrack(id).then((data) => {
        track.DURATION_MS = data.body.duration_ms;
        track.ARTISTS = data.body.artists;
    });
}
//for the new config (for old config use updateTime)
var updateTime = config.spotifyUpdateTime;

function updateSong() {
    if (stop) return;
    try {
        spotifyApi.getMyCurrentPlayingTrack().then(function (data) {


            if (data === undefined || data.body === undefined || data.body.item === undefined || data.body.item.name === undefined || data.body.progress_ms === undefined) {
                sendData("game_event", {
                    "game": "SPOTIFY_UO",
                    "event": "SC_PROGRESS_UPDATE",
                    "data": {
                        "value": 0,
                        "frame": {
                            "first-line": "Unknown Title",
                        }
                    }
                });
                setTimeout(() => {
                    updateSong();
                }, updateTime);
                return;
            }
            var progress_ms = data.body.progress_ms
            var name = data.body.item.name;
            track.NAME = name;
            if (data.body.is_playing) {
                updateTrack(data.body.item.id);

                //console.log(progress_ms)
                var artistsString = "";
                if (track.ARTISTS.length === 1) {
                    if (track.ARTISTS[0] !== undefined && track.ARTISTS[0].name !== undefined) artistsString = track.ARTISTS[0].name
                } else if (track.ARTISTS.length > 0) {
                    for (var i = 0; i < track.ARTISTS.length; i++) {
                        if (i === 0) {
                            if (track.ARTISTS[i] !== undefined && track.ARTISTS[i].name !== undefined) artistsString += track.ARTISTS[i].name
                        } else {
                            if (track.ARTISTS[i] !== undefined && track.ARTISTS[i].name !== undefined) artistsString += ", " + track.ARTISTS[i].name
                        }
                    }
                } else {
                    artistsString = undefined;
                }

                var line;
                if (artistsString === undefined) {
                    line = getString(name);
                } else {
                    line = getString(name + " -- " + artistsString);
                }


                var perc = Math.floor((progress_ms / track.DURATION_MS) * 100);
                track.PROGRESS = perc;
                sendData("game_event", {
                    "game": "SPOTIFY_UO",
                    "event": "SC_PROGRESS_UPDATE",
                    "data": {
                        "value": perc,
                        "frame": {
                            "first-line": line,
                        }
                    }
                });
                setTimeout(() => {
                    updateSong();
                }, updateTime);


            } else {
                var artistsString = "";
                if (track.ARTISTS.length === 1) {
                    if (track.ARTISTS[0] !== undefined && track.ARTISTS[0].name !== undefined) artistsString = track.ARTISTS[0].name
                } else if (track.ARTISTS.length > 0) {
                    for (var i = 0; i < track.ARTISTS.length; i++) {
                        if (i === 0) {
                            if (track.ARTISTS[i] !== undefined && track.ARTISTS[i].name !== undefined) artistsString += track.ARTISTS[i].name
                        } else {
                            if (track.ARTISTS[i] !== undefined && track.ARTISTS[i].name !== undefined) artistsString += ", " + track.ARTISTS[i].name
                        }
                    }
                } else {
                    artistsString = undefined;
                }
                var line;
                if (artistsString === undefined) {
                    line = getString(track.NAME);
                } else {
                    line = getString(track.NAME + " -- " + artistsString);
                }

                sendData("game_event", {
                    "game": "SPOTIFY_UO",
                    "event": "SC_PROGRESS_UPDATE",
                    "data": {
                        "value": track.PROGRESS,
                        "frame": {
                            "first-line": line,
                        }
                    }
                });
                setTimeout(() => {
                    updateSong();
                }, updateTime);
            }


        }, function (err) {
            console.log('Something went wrong!', err);
        });
    } catch (e) {
        console.log(e);
        console.info("You can ignore this error /\\");
        setTimeout(() => {
            updateSong();
        }, updateTime);
    }
}


function registerEngine() {
    sendData("game_metadata", {"game": "SPOTIFY_UO", "game_display_name": "Spotify", "developer": "Unbemerkt"});
    sendData("bind_game_event", {
        "game": "SPOTIFY_UO",
        "event": "SC_PROGRESS_UPDATE",
        "handlers": [
            {
                "device-type": config.screenSize,
                "zone": "one",
                "mode": "screen",
                "datas": [
                    {
                        "lines": [
                            {
                                "has-text": true,
                                "context-frame-key": "first-line",
                            },
                            {
                                "has-progress-bar": true
                            }
                        ]
                    }
                ]
            }
        ]
    })

    config.isRegistered = true;
    fs.writeFileSync("./config.json", JSON.stringify(config, null, 2), "utf8");
}

function sendData(endPoint, dataObj) {
    var authOptions = {
        json: dataObj
    };
    request.post("http://" + steelSeriesCfg.address + "/" + endPoint, authOptions, (error, response, body) => {
        //console.log(body)
        //console.log(error)
    })
}


