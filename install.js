const fs = require("fs");
console.log("Searching 'coreProps.json'...");
var isDarwin = process.platform === 'darwin';
fs.copyFileSync("./config-example.json","./config.json");
var json = JSON.parse(fs.readFileSync("./config.json", {encoding: "utf8", flag: "r"}));
if (isDarwin){
    //do apple stuff
    json.coreProps_PATH = "/Library/Application Support/SteelSeries Engine 3/coreProps.json";
}else{
    //do win stuff
    json.coreProps_PATH = process.env.ProgramData+"\\SteelSeries\\SteelSeries Engine 3\\coreProps.json";
}
if (fs.existsSync(json.coreProps_PATH)){
    console.log("'coreProps.json' found!");
    fs.writeFileSync("./config.json",JSON.stringify(json,null,2));
    console.log("Config saved!");
}else{
    console.log("'coreProps.json' not found! Please add the path to the 'coreProps.json' manually!");
}
console.log("Closing in 5 seconds...")
setTimeout(()=>{
    console.log("bb")
},5*1000)