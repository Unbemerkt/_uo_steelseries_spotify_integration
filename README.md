# [UO]SteelSeries » Spotify Integration 1.0.4
#### Information:
This project is not an Official project from Spotify or SteelSeries!
It is only intended for private use!



**Installation**
---
This program needs [node.js](https://nodejs.org/en/)
1. download all files
2. run `npm install`  or run the `install.bat` on windows (on OSX `install.sh`)
3. create an application on [Spotify](https://developer.spotify.com/dashboard/applications)
4. go to your app and click on `EDIT SETTINGS` go to `Redirect URIs` and add the following URL's: `http://127.0.0.1/login_confirm/` `http://127.0.0.1/` (or if you have set another port than `80` then use `http://127.0.0.1:PORT/login_confirm/` `http://127.0.0.1:PORT/`) click on save
5. insert the cliendID and the clientSecret of your application into the `config.json`
6. select your device from the list [2] and copy `screened-XxY` of your device in the `config.json` at `screenSize`
7. (only if the installer has not found the file) insert the path of the `coreProps.json`[1]
8. run `npm run start` or run the `start.bat` on windows (on OSX `start.sh`)



[1] **OSX | `/Library/Application Support/SteelSeries Engine 3/coreProps.json`**

[1] **Windows | `%PROGRAMDATA%/SteelSeries/SteelSeries Engine 3/coreProps.json`**



**Uninstallation**
---
1. launch the program
2. go to `https://127.0.0.1/settings` in your browser
3. Slide `Remove SSE App` to the right
4. close the browser window
5. delete all files or run `remove.bat` on windows (on OSX `remove.sh`)


**[2]Compatible with[*]**
---
* `screened-128x36`: Rival 700, Rival 710
* `screened-128x40`: Apex 7, Apex 7 TKL, Apex Pro, Apex Pro TKL
* `screened-128x48`: Arctis Pro Wireless
* `screened-128x52`: GameDAC / Arctis Pro + GameDAC

[*] Only tested with Apex Pro

### Config ###
---
* `port` »  on which port should the interface run (default 80)
* `clientID` » The client id of your Spotify application
* `clientSecret` » The client secret of your Spotify application
* `coreProps_PATH` » The path of the `coreProps.json`[1]
* `isRegistered` » do not edit this!
* `screenSize` » look [2]
* `showArtist` » should the artist be displayed
* `autoStart` » if you are logged in, the program will be loaded automatically
* `screenUpdateTime` » at which interval should the display be updated (recommended 2000ms) in ms
* `spotifyUpdateTime` » at which interval should the song information be updated (recommended 2000ms) in ms
* `automaticReloadWhenError` » when an error occurred should the app be automatically reloaded?


### License ###
---
```
MIT License

Copyright (c) 2020-2023 Keno Schulz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE
